Notes to creator of new sprint planning issue:
- Modify the dates below to signify the correct year, quarter, and sprint dates.
- The title of the issue should be in the format `FY__Q_ Engineering Analytics Sprint Planning: Sprint X (YYYY-MM-DD - YYYY-MM-DD)`

---

**This is the issue for sprint planning for _FY2XQXX, Sprint XX of 6 (YYYY-MM-DD to YYYY-MM-DD)_.**

- Please edit this issue's description to add links to issues from team-tasks that you would like to add to the upcoming sprint. 
  - If you want to add a task or request, and there is no issue for it yet, create a new issue, and then add the link for the newly created issue below.
- Discussion about proposed tasks will take place in comments on this issue.
- This planning issue will be closed on or before the first day of the sprint.

**Issues proposed for upcoming sprint:**
- [ ] [Example issue #1](https://gitlab.com/gitlab-org/quality/engineering-analytics/team-tasks/-/issues/71) 
- [ ] [Example issue #2](https://gitlab.com/gitlab-org/quality/engineering-analytics/team-tasks/-/issues/71) 
