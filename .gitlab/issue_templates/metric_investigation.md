## Metric Investigation

### Description

Please provide us some context by answering the following questions:

- Where and when did you discover this data quality problem? Please provide links to the charts.

- If possible, can you provide what the correct values/numbers should be?

- Is this issue related to any other existing data quality issues? If yes, please provide Issue links.

### Outcome

- Is there a deadline when this issue needs to be fixed?
- Where is the SSOT for this metric that we should compare the result with?

### Additional comments