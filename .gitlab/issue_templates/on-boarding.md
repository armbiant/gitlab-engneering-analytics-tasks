# Engineering Analytics Team-specific Onboarding

- [ ] Name this issue `[YOUR_NAME] Onboarding for Engineering Analytics`
- [ ] Manager: to assign Engineering Department stable counterpart `TBD`

## General Team Information

Important handbook pages & information on how we work

- [ ] Review the Engineering Analytics team Handbook page.
  - https://about.gitlab.com/handbook/engineering/quality/engineering-analytics/ 
- [ ] Review the Engineering Metrics Handbook page.
  - https://about.gitlab.com/handbook/engineering/metrics/ 
- [ ] Once you have been assigned your stable counterparts by your manager, create an MR and add yourself to stable counterparts table in the Handbook.
  - https://about.gitlab.com/handbook/engineering/quality/engineering-analytics/#counterpart-assignments 
- [ ] Familiarize yourself with the Engineering Analytics Team board
  - https://gitlab.com/gitlab-org/quality/engineering-analytics/team-tasks/-/boards/2650515?group_by=epic
  - This is the team task dashboard, grouped by epics. Each epic corresponds roughly to either a planned objective for the current quarter, or to ad-hoc analytics work that we receive from Engineering departments throughout the quarter.
- [ ] Review the Department Performance Indicator pages. 
  - There is no immediate need to review and understand all metrics, but it is a good idea to become at least familiar with these metrics over time. The key performance indicators ("KPIs") are the most important metrics for each department. 
  - You get bonus points if you propose a change or find an error on a page. :)
    - [ ] Infrastructure Department: https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators 
    - [ ] Security Department: https://about.gitlab.com/handbook/engineering/security/performance-indicators/
    - [ ] Development Department: https://about.gitlab.com/handbook/engineering/development/performance-indicators/
    - [ ] Support Department: https://about.gitlab.com/handbook/support/performance-indicators/
    - [ ] Quality Department: https://about.gitlab.com/handbook/engineering/quality/performance-indicators/
    - [ ] UX Department: https://about.gitlab.com/handbook/engineering/ux/performance-indicators/
    - [ ] Incubation Department: https://about.gitlab.com/handbook/engineering/incubation/performance-indicators/
- [ ] Review guidelines for creating and updating charts in Sisense. 
  - https://about.gitlab.com/handbook/engineering/metrics/#guidelines 

####  FinOps Specialty

Additional materials for Engineering Analyst (FinOps): 
- [ ] Review SaaS Free User Efficiency Internal Handbook 
  - https://internal-handbook.gitlab.io/handbook/product/saas-efficiency/ 
- [ ] Review previous Infrafin Board: 
  - https://gitlab.com/groups/gitlab-com/-/boards/1502173?label_name[]=infrafin
- [ ] Review Cost Management Handbook Page: 
  - https://about.gitlab.com/handbook/engineering/infrastructure/cost-management/
- [ ] Review costs spreadsheet and familiarize yourself with how the spreadsheet is calculated 
  - https://docs.google.com/spreadsheets/d/1UQ2Vuo0wZUtbeSiKhnKpuID-fHTPcQadJslbC2i3KTA/edit#gid=1230668564 
- [ ] Review Data Studio Dashboards. This may be unnecessary if this data is in Sisense by the time of onboarding. 
  - https://datastudio.google.com/u/0/reporting/30db397d-50a7-460c-a1f9-439ec79a0433/page/ujBhC, https://datastudio.google.com/reporting/02982d5f-04b0-4cdb-b4eb-3562e5cdffcf/page/ujBhC. 
  - Access requests may need to be granted. See "Access Requests" section below.

## Access Requests 
- [ ] Check if you have editor access in Sisense. This gives you the ability to view underlying SQL and create charts. 
  - You can check by going to any Sisense dashboard. On the top right hand corner of the chart, you should see a pencil icon. 
  - If you do not, request editor access to Sisense via issue.
  - Example access request issue: https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/4005
- [ ] Check with your manager if you need access to SAFE dashboards. Another AR will need to be submitted to access SAFE information.

#### FinOps Specialty

Additional access needed for Engineering Analyst (FinOps):
- [ ] Ability to query from gitlab-production project in BQ and view/edit Data Studio dashboards. Request to be added to groups 
  - `gcp-scalability-backend-eng-sg` 
  - `gcp-prod-bigdata-sg`
  - See the below examples 
    - https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/14272
    - https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/13075
    - https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/13099 
- [ ] Ability to view data from the GCP Billing console. Request to be a part of `gcp-billing-sg`. 
  - Example access request issue: https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/15107

## Triage rotation
- [ ] Review Data Triage Program and add yourself to schedule https://about.gitlab.com/handbook/business-technology/data-team/how-we-work/triage/. Reach out to Israel Weeks (`@iweeks`) in the [#data team Slack channel](https://gitlab.slack.com/archives/C8D1LGC23) to be added.

## Slack channels

Join these Slack channels:
- [ ] #eng-data-kpi
- [ ] #g-engineering-analytics
- [ ] #data-lounge
- [ ] #functional_analytics_center_of_excellence
- [ ] #development_metrics
- [ ] #data
- [ ] #wg_next-prioritization

#### FinOps Specialty

Additional important channels  for Engineering Analyst (FinOps):
- [ ] #i_efficient_free_tier
- [ ] #infrafin
- [ ] #ra-ssot-cost-data


## On-boarding 1-1s
- [ ] Schedule 30 min 1:1 with your manager, if it is not already on your calendar. This should be a recurring weekly 1:1.
- [ ] Schedule 30 min 1:1 with Lily Mai (`@lmai`), Staff Engineering Analyst, or Raul Rendon (`@raulrendon`), Senior Engineering Analyst,  to understand more about the team and how we work
- [ ] Schedule 30 min 1:1 with Peter Empey (`@pempey`), Senior Analytics Engineer, to understand our interactions with the [Data Team](https://about.gitlab.com/handbook/business-technology/data-team/)
- [ ] Schedule 30 min 1:1 with VP of your counterpart Engineering Department to introduce yourself

###  FinOps Specialty

After gaining basic of understanding of all the infra metrics and discussing each topic with manager, then do these tasks:
- [ ] Schedule 30 min meeting with Joshua Lambert (`@joshlambert`) to understand previous and upcoming free user strategy data asks
- [ ] Schedule 30 min meeting with Jonathan Miller Infrastructure FP&A (`@JonathanMiller`) to understand billing spreadsheet & discuss top priorities, cost/user metrics, financial forecast, etc.
- [ ] Schedule 30 min meeting with VP of infrastructure discuss what top priorities are currently and expected in future
- [ ] Schedule 30 min meeting with Director of infra, Reliability to discuss top priorities and metrics related to incidents & Uptime of GitLab.com
- [ ] Schedule 30 min meeting with Director/Senior Manager of infra, Scalability to discuss top priorities and metrics related to deployment times

## Recurring events 

### Engineering Key review meetings
Request to be invited to these meetings. Feel free to click into the meeting agendas (can be found on calendar invites) to read the most recent conversations.
- [ ] Security Key Review
- [ ] UX Key Review
- [ ] Development Key Review
- [ ] Quality Key Review
- [ ] Infrastructure Key Review
- [ ] Support Key Review

### Engineering team meetings
- [ ] Engineering Analytics Team Meeting (Every Thursday)
- [ ] GitLab x Data Sync (Every Thursday)
- [ ] SaaS Free User Efficiency (Every Monday and Wednesday)
- [ ] Engineering Allocation (Every Tuesday)

### Others
- [ ] Data Triage schedule (your assigned day is dependent on schedule)

## Tasks
- [ ] Update this section with additional checklist items you completed during onboarding for next Engineering Analyst 
- [ ] Pick your first task on the Engineering Metrics board https://gitlab.com/gitlab-com/www-gitlab-com/-/boards/1942495?label_name[]=Engineering%20Metrics
